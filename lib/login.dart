import 'dart:developer';

import 'package:mooncycles/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => AddLoginState();
}

  class AddLoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    TextEditingController usernameController = TextEditingController();
    TextEditingController passwordController = TextEditingController();

    Future _addLogin() {
      final username = usernameController.text;
      final password = passwordController.text;

      final ref = FirebaseFirestore.instance.collection("Login").doc();

      return ref
      .set({"Username": username, "Password": password})
      .then((value) => log("Login successful!"))
      .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 16,
              ),
              const Text(
                "Login",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              TextField(
                controller: usernameController,
                decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.person,
                    size: 24,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText: "Username",
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              TextField(
                controller: passwordController,
                decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.lock_outline,
                    size: 24,
                  ),
                  suffixIcon: const Icon(
                    Icons.visibility,
                    size: 24,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText: "Password",
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              Row(children: [
                ElevatedButton(
                  onPressed: () {
                    _addLogin();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const Dashboard()),
                    );
                  },
                  child: const Text("Login"),
                ),
              ])
            ],
          ),
        ),
      ),
    );
  }
}

