import 'dart:developer';

import 'package:mooncycles/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => AddRegistrationState();
}

  class AddRegistrationState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    TextEditingController emailController = TextEditingController();
    TextEditingController newUsernameController = TextEditingController();
    TextEditingController newPasswordController = TextEditingController();
    TextEditingController repeatPasswordController = TextEditingController();
    
    Future _addRegistration() {
      final email = emailController.text;
      final newUsername = newUsernameController.text;
      final newPassword = newPasswordController.text;
      final repeatPassword = repeatPasswordController.text;

      final ref = FirebaseFirestore.instance.collection("Registration").doc();

      return ref
      .set({"Email": email, "Username": newUsername, "Password": newPassword, "Repeat password": repeatPassword})
      .then((value) => log("Registration successful!"))
      .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Register your account"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 24,
        ),
        child: Column(
          children: [
            const Text(
              "Create an account with MoonCycle",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.email,
                  size: 24,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Please enter your email address",
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              controller: newUsernameController,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.person,
                  size: 24,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Select your profile username",
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              obscureText: true,
              controller: newPasswordController,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.lock,
                  size: 24,
                ),
                suffixIcon: const Icon(
                  Icons.visibility,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Enter your new password",
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            TextField(
              obscureText: true,
              controller: repeatPasswordController,
              decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.lock,
                  size: 24,
                ),
                suffixIcon: const Icon(
                  Icons.visibility,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                hintText: "Repeat your password",
              ),
            ),
            const SizedBox(
              height: 12,
            ),
            ElevatedButton(
              onPressed: () {
                _addRegistration();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                );
              },
              child: const Text("Register"),
            ),
          ],
        ),
      ),
    );
  }
}


